import java.util.HashMap;

public class Dish {
    private String name;
    private final int id;
    private HashMap<Ingredient, Integer> ingredients;

    public Dish(String name, HashMap<Ingredient, Integer> ingredients) {
        this.name = name;
        id = (int)(Math.random()*1000);
        this.ingredients = ingredients;
    }

    public void addIngredient(Ingredient ingredient, Integer amount) {
        ingredients.put(ingredient, amount);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public HashMap<Ingredient, Integer> getIngredients() {
        return ingredients;
    }

    public void rename(String name) {
        this.name = name;
    }
}

public class Client {
    private final int insuranceId;
    private final String name;
    private final String address;

    public Client(int insuranceId, String name, String address) {
        this.insuranceId = insuranceId;
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public int getInsuranceId() {
        return insuranceId;
    }

    public String getAddress() {
        return address;
    }
}

public class Table {
    private final int id;
    private final int maxPeople;

    public Table(int maxPeople) {
        id = (int)(Math.random()*1000);
        this.maxPeople = maxPeople;
    }

    public int getId() {
        return id;
    }

    public int getMaxPeople() {
        return maxPeople;
    }
}

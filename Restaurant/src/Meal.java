import java.sql.Time;
import java.util.Date;
import java.util.HashMap;

public class Meal {
    private Date date;
    private Time startTime;
    private Time endTime;
    private Waiter waiter;
    private Table table;
    private HashMap<Dish, Integer> dishes;
    private HashMap<Client, Integer> clients;

    public Meal(Date date, Time startTime, Time endTime, Waiter waiter, Table table) {
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.waiter = waiter;
        this.table = table;
        this.dishes = new HashMap<Dish, Integer>();
        this.clients = new HashMap<Client, Integer>();
    }

    public Date getDate() {
        return date;
    }

    public Time getStartTime() {
        return startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public Waiter getWaiter() {
        return waiter;
    }

    public Table getTable() {
        return table;
    }

    public HashMap<Dish, Integer> getDishes() {
        return dishes;
    }

    public HashMap<Client, Integer> getClients() {
        return clients;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public void setWaiter(Waiter waiter) {
        this.waiter = waiter;
    }

    public void addClient(Client client, Integer amount) {
        clients.put(client, amount);
    }

    public void addDish(Dish dish, Integer amount) {
        dishes.put(dish, amount);
    }
}

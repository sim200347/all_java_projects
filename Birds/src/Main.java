import java.awt.*;
import java.util.Locale;

public class Main {
    public static void addItem(int size, Group group, String name) {
        for (int i = 0; i < size; i++) {
            switch (name.strip().toLowerCase(Locale.ROOT).replaceAll("[ ]+","")) {
                case "parrot" -> {group.add(new Parrot());}
                case "penguin" -> {group.add(new Penguin());}
                case "sparrow" -> {group.add(new Sparrow());}
            }
        }
    }

    public static void main(String[] args) {
        Group group = new Group();
        addItem(3, group, "parrot");
        addItem(3, group, "penguin");
        addItem(3, group, "sparrow");
        Frame frame = new Frame(group);
        Parrot.printCount();
        Penguin.printCount();
        Sparrow.printCount();
    }
}
import java.awt.*;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Sparrow extends Bird{
    private static int count;
    private static int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
    private static int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;

    public Sparrow() {
        count++;
        size = 6;
        color = new Color(118, 107, 107);
        updateMinAndMax();
    }

    private void updateMinAndMax() {
        maxX = max(maxX, x);
        maxY = max(maxY, y);
        minX = min(minX, x);
        minY = min(minY, y);
    }

    public static void printCount() {
        System.out.println("Sparrow: " + count);
    }

    public static void drawRectangleOfBirds(Graphics graphics) {
        graphics.setColor(Color.gray);
        System.out.println(maxX + " " + maxY + " " + minX + " " + minY);
        graphics.drawRect(minX - size/2, minY - size/2, (maxX - minX + size), (maxY - minY + size));
    }
}

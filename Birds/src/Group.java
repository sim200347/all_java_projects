import java.awt.*;
import java.util.ArrayList;

public class Group extends Bird {
    private final ArrayList<Bird> birds;

    public Group() {
        birds = new ArrayList<Bird>();
    }

    public void add (Bird bird) {
        birds.add(bird);
    }

    public void draw(Graphics graphics) {
        for (Bird bird : birds) {
            bird.draw(graphics);
        }
    }
}

import java.awt.*;

import static java.lang.Math.max;
import static java.lang.Math.min;

public abstract class Bird {
    protected int x, y;
    protected static int size;
    protected Color color;

    private static int count;
    private static int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
    private static int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;


    public Bird() {
        System.out.println("I'm a bird");
        count++;
        x = (int) (Math.random() * Frame.width - Frame.width/2);
        y = (int) (Math.random() * Frame.height - Frame.height/2);
        System.out.println(x + " " + y);
        size = 10;
        color = Color.BLACK;
        updateMinAndMax();
    }

    private void updateMinAndMax() {
        maxX = max(maxX, x);
        maxY = max(maxY, y);
        minX = min(minX, x);
        minY = min(minY, y);
    }

    public void fly() {
        System.out.println("I'm flying bird");
    }

    public static void printCount() {
        System.out.println("Birds: " + count);
    }

    public void draw(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillOval(x-size/2, -y-size/2, size, size);
    }

    public static void drawRectangleOfBirds(Graphics graphics) {
        graphics.setColor(Color.black);
        System.out.println(maxX + " " + maxY + " " + minX + " " + minY);
        graphics.drawRect(minX - size/2, minY - size/2, maxX - minX + size, maxY - minY + size);
    }
}
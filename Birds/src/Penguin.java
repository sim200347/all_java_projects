import java.awt.*;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Penguin extends Bird {
    private static int count;
    private static int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
    private static int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;

    public Penguin() {
        System.out.println("I'm a penguin");
        count++;
        size = 20;
        color = Color.BLACK;
        updateMinAndMax();
    }

    private void updateMinAndMax() {
        maxX = max(maxX, x);
        maxY = max(maxY, y);
        minX = min(minX, x);
        minY = min(minY, y);
    }

    @Override
    public void fly() {
        System.out.println("Penguins can't fly");
    }

    public void hello(Penguin penguin) {
        System.out.println("Hi, penguin");
    }

    public void hello(Parrot parrot) {
        System.out.println("Hi, parrot " + parrot.getName());
    }

    public static void printCount() {
        System.out.println("Penguins: " + count);
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillOval(x-size/2, -y-size/2, size, size);
        graphics.setColor(new Color(179, 179, 179));
        graphics.fillOval(x-size/4, -y-size/4, size/2, size/2);
    }

    public static void drawRectangleOfBirds(Graphics graphics) {
        graphics.setColor(Color.blue);
        System.out.println(maxX + " " + maxY + " " + minX + " " + minY);
        graphics.drawRect(minX - size/2, minY - size/2, maxX - minX + size, maxY - minY + size);
    }
}
import java.awt.*;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Parrot extends Bird {
    private String name;

    private static int count;
    private static int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
    private static int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;

    public Parrot() {
        System.out.println("I'm parrot");
        count++;
        size = 9;
        color = new Color(105, 191, 167);
        updateMinAndMax();
    }

    private void updateMinAndMax() {
        maxX = max(maxX, x);
        maxY = max(maxY, y);
        minX = min(minX, x);
        minY = min(minY, y);
    }

    public Parrot(String name) {
        this();
        this.name = name;
        size = 25;
        color = new Color(105, 191, 167);
    }
    
    public void speak() {
        System.out.println("I'm " + name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static void printCount() {
        System.out.println("Parrots: " + count);
    }

    public static void drawRectangleOfBirds(Graphics graphics) {
        graphics.setColor(Color.green);
        System.out.println(maxX + " " + maxY + " " + minX + " " + minY);
        graphics.drawRect(minX - size/2, minY - size/2, (maxX - minX + size), (maxY - minY + size));
    }
}
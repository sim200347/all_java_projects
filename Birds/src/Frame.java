import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {
    public static final int width = 400, height = 400;
    private Group birds = null;
    private Bird bird = null;

    public Frame(Group birds) {
        this.birds = birds;
        initFrame();
    }

    public Frame(Bird bird) {
        this.bird = bird;
        initFrame();
    }

    private void initFrame() {
        setSize(width, height);
        setLocation((1920 - width) / 2, (1080 - height) / 2);
        setTitle("Birds");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void paint(Graphics graphics) {
        graphics.translate(width / 2, height / 2);
        graphics.drawLine(0, height / 2, 0, -height / 2);
        graphics.drawLine(-width / 2, 0, width / 2, 0);
        birds.draw(graphics);
        Bird.drawRectangleOfBirds(graphics);
        Sparrow.drawRectangleOfBirds(graphics);
        Parrot.drawRectangleOfBirds(graphics);
        Penguin.drawRectangleOfBirds(graphics);
    }
}
import java.util.*;

public class L1ListNode {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();
        Scanner in = new Scanner(System.in);
        boolean flag = true;
        while (flag) {
            String input = in.nextLine();
            if (input.equals("q")) {
                flag = false;
                continue;
            }
            System.out.println(set.add(input) ? "No" : "Yes");
        }
        TreeMap<Integer, String> map = new TreeMap<>();
        map.put(9, "999");
        map.put(25, "25");
        System.out.println(map);
    }
}
import java.util.Scanner;

public class TestRegex {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext())
            System.out.println(in.nextLine().matches("^[^\\W]+@[^\\W]+(\\.[^\\W]{2,3})+$"));
    }
}

import java.util.HashMap;
import java.util.Scanner;

public class MapUsage {
    public static String MENU = "1. Add number\n2. Delete person\n3. Find person by last name\n\nTo exit, type q";
    public static HashMap<String, String> telephones = new HashMap<>();
    public static Scanner in = new Scanner(System.in);

    public static void addNumber() {
        System.out.println("Type last name and phone number (+7) with space");
        String input = in.nextLine();
        String[] data = input.split(" ");
        telephones.put(data[1].replaceAll("[() -]", ""), data[0]);
    }

    public static void deleteNumber() {
        System.out.println("Type phone number (+7) to delete person");
        String input = in.nextLine();
        telephones.remove(input.replaceAll("[() -]", ""));
    }

    public static void findPerson() {
        System.out.println("Type phone number (+7) to find person");
        String input = in.nextLine();
        System.out.println(telephones.get(input.replaceAll("[() -]", "")));
    }

    public static void main(String[] args) {
        boolean flag = true;
        while (flag) {
            System.out.println(MENU);
            String input = in.nextLine();
            if (input.equals("q")) {
                flag = false;
                continue;
            }
            switch (input) {
                case "1" -> {
                    addNumber();
                }
                case "2" -> {
                    deleteNumber();
                }
                case "3" -> {
                    findPerson();
                }
            }
        }
    }
}

import org.junit.*;

import static org.junit.Assert.*;

public class SimpleClassTest {
    static int i;

    @Test(timeout=10)
    public void testMin() {
        assertEquals(3, SimpleClass.min(3, 5, 4));
//        assertEquals("Тест не пройден.", 30, SimpleClass.min(3, 5, 4));
        assertEquals(5, SimpleClass.min(5, 5, 5));
        assertEquals(2, SimpleClass.min(2, 2, 2));
//        while (true){}
    }

    @Ignore
    @Test
    public void testConstructor() {
        SimpleClass obj = new SimpleClass(100);
        assertEquals(100, obj.getAttr());
    }

    @Test
    public void testDiv() {
        SimpleClass obj = new SimpleClass(100);
        assertEquals(10, obj.div(10));
        assertEquals(20, obj.div(5));
        assertEquals(0, obj.div(200));
        assertEquals(1, obj.div(100));
    }

    @Test(expected = Exception.class)
    public void testDivException() {
        new SimpleClass(100).div(0);
    }

    @AfterClass
    @BeforeClass
    public static void f() {
        i = -1;
        System.out.println(i);
    }
}
import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;

public class CubeFrame extends JFrame {
    private final int height = 400;
    private final int width = 400;
//    private final BufferStrategy strategy;
    private final CubePanel cubePanel;

    public CubeFrame(Cube cube) {
        this.setTitle("Cube");
        this.setSize(width, height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
//        this.setResizable(false);
//        this.createBufferStrategy(2);
//        strategy = this.getBufferStrategy();
        Container pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        CubePanel cubePanel = new CubePanel(cube, width, height);
        this.cubePanel = cubePanel;
        cubePanel.setPreferredSize(new Dimension(width - 50, height - 50));
        CubeController cubeController = new CubeController(cube, pane, cubePanel);
        pane.add(cubePanel, BorderLayout.CENTER);
        this.pack();
        cubePanel.requestFocusInWindow();
        this.setVisible(true);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
//        g = strategy.getDrawGraphics();
//        cubePanel.paint(g);
        g.dispose();
//        strategy.show();
    }

    public void repaint(Graphics g) {
        g.clearRect(0, 0, width, height);
        paint(g);
    }
}

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyTransparencyButton extends MyButton {

    public MyTransparencyButton(Cube cube, JPanel panel) {
        super(cube, panel);
        MyButtonListener myButtonListener = new MyButtonListener();
        this.addActionListener(myButtonListener);
        this.setText("Transparency");
    }

    private class MyButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            cube.changeTransparency();
            panel.repaint();
        }
    }
}

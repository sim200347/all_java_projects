import java.awt.*;
import java.awt.geom.Path2D;

public class Facet {
    private R3Vector[] vertex;
    private Color color;

    public Facet(R3Vector... vectors) {
        color = Color.black;
        vertex = new R3Vector[4];
        vertex = vectors;
    }

    public void scale(double scale) {
        for (R3Vector vector : vertex) {
            vector.scale(scale);
        }
    }

    public void translate(double dx, double dy, double dz) {
        for (R3Vector vector : vertex) {
            vector.translate(dx, dy, dz);
        }
    }

    public void rotate(double ux, double uy, double uz) {
        for (R3Vector vector : vertex) {
            vector.rotate(ux, uy, uz);
        }
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void draw(Graphics g, int isTransparent, int isOrthographic) {
        if (isOrthographic > 0)
            ortDraw(g, isTransparent);
        else
            perspectiveDraw(g, isTransparent);
    }

    private void ortDraw(Graphics g, int isTransparent) {
        Path2D p = new Path2D.Double();
        p.moveTo(vertex[0].getX(), vertex[0].getY());
        p.lineTo(vertex[1].getX(), vertex[1].getY());
        p.lineTo(vertex[2].getX(), vertex[2].getY());
        p.lineTo(vertex[3].getX(), vertex[3].getY());
        p.lineTo(vertex[0].getX(), vertex[0].getY());
        p.closePath();
        if (isTransparent > 0) {
            if (R3Vector.vectorMultiplication(R3Vector.shiftToZero(vertex[0], vertex[1]), R3Vector.shiftToZero(vertex[1], vertex[2])).getZ() > 0) {
                g.setColor(color);
                ((Graphics2D) g).fill(p);
            }
        } else {
            g.setColor(color);
            ((Graphics2D) g).draw(p);
        }
    }

    private void perspectiveDraw(Graphics g, int isTransparent) {
        int c = -999;
        Path2D p = new Path2D.Double();
        p.moveTo(vertex[0].getX() * (c / (c - vertex[0].getZ())), vertex[0].getY() * (c / (c - vertex[0].getZ())));
        p.lineTo(vertex[1].getX() * (c / (c - vertex[1].getZ())), vertex[1].getY() * (c / (c - vertex[1].getZ())));
        p.lineTo(vertex[2].getX() * (c / (c - vertex[2].getZ())), vertex[2].getY() * (c / (c - vertex[2].getZ())));
        p.lineTo(vertex[3].getX() * (c / (c - vertex[3].getZ())), vertex[3].getY() * (c / (c - vertex[3].getZ())));
        p.lineTo(vertex[0].getX() * (c / (c - vertex[0].getZ())), vertex[0].getY() * (c / (c - vertex[0].getZ())));
        p.closePath();
        R3Vector vertex0 = new R3Vector(vertex[0].getX() * (c / (c - vertex[0].getZ())), vertex[0].getY() * (c / (c - vertex[0].getZ())), vertex[0].getZ());
        R3Vector vertex1 = new R3Vector(vertex[1].getX() * (c / (c - vertex[1].getZ())), vertex[1].getY() * (c / (c - vertex[1].getZ())), vertex[1].getZ());
        R3Vector vertex2 = new R3Vector(vertex[2].getX() * (c / (c - vertex[2].getZ())), vertex[2].getY() * (c / (c - vertex[2].getZ())), vertex[2].getZ());
        if (isTransparent > 0) {
            if (R3Vector.vectorMultiplication(R3Vector.shiftToZero(vertex0, vertex1), R3Vector.shiftToZero(vertex1, vertex2)).getZ() > 0) {
                g.setColor(color);
                ((Graphics2D) g).fill(p);
            }
        } else {
            g.setColor(color);
            ((Graphics2D) g).draw(p);
        }
    }
}

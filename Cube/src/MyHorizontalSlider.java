import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MyHorizontalSlider extends MySlider {

    public MyHorizontalSlider(Cube cube, JPanel panel) {
        super(cube, panel);
        MySliderListener mySliderListener = new MySliderListener();
        this.addChangeListener(mySliderListener);
        setOrientation(SwingConstants.HORIZONTAL);
    }

    private class MySliderListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            MySlider slider = (MySlider) e.getSource();
            cube.rotateHorizontally(slider.getValue());
        }
    }
}

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyOrtButton extends MyButton {
    public MyOrtButton(Cube cube, JPanel panel) {
        super(cube, panel);
        MyButtonListener myButtonListener = new MyButtonListener();
        this.addActionListener(myButtonListener);
        this.setText("Orthogonal");
    }

    private class MyButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            cube.changeOrth();
            panel.repaint();
        }
    }
}

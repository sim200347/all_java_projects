import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MyVerticalSlider extends MySlider {
    public MyVerticalSlider(Cube cube, JPanel panel) {
        super(cube, panel);
        MySliderListener mySliderListener = new MySliderListener();
        this.addChangeListener(mySliderListener);
        setOrientation(SwingConstants.VERTICAL);
    }

    private class MySliderListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            MySlider slider = (MySlider) e.getSource();
            cube.rotateVertically(slider.getValue());
        }
    }
}

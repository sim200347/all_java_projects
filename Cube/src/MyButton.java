import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class MyButton extends JButton implements Observer {
    protected Cube cube;
    protected JPanel panel;

    public MyButton(Cube cube, JPanel panel) {
        this.cube = cube;
        this.panel = panel;
    }

    @Override
    public void update(Observable o, Object arg) {
        cube = (Cube) o;
        panel.repaint();
    }
}

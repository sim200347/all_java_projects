import static java.lang.Math.*;

public class R3Vector {
    private double x, y, z;

    R3Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static R3Vector vectorMultiplication(R3Vector vector1, R3Vector vector2) {
        return new R3Vector(vector1.y * vector2.z - vector1.z * vector2.y, vector1.z * vector2.x - vector1.x * vector2.z, vector1.x * vector2.y - vector1.y * vector2.x);
    }

    public static R3Vector shiftToZero(R3Vector begin, R3Vector end) {
        return new R3Vector(end.x - begin.x, end.y - begin.y, end.z - begin.z);
    }

    public void scale(double scale) {
        x *= scale;
        y *= scale;
        z *= scale;
    }

    public void translate(double dx, double dy, double dz) {
        x += dx;
        y += dy;
        z += dz;
    }

    public void rotate(double ux, double uy, double uz) {
        rotateX(ux);
        rotateY(uy);
        rotateZ(uz);
    }

    private void rotateX(double u) {
        u = toRadians(u);
        double tmp = y;
        y = tmp * cos(u) + z * sin(u);
        z = -tmp * sin(u) + z * cos(u);
    }

    private void rotateY(double u) {
        u = toRadians(u);
        double tmp = x;
        x = tmp * cos(u) + z * sin(u);
        z = -tmp * sin(u) + z * cos(u);
    }

    private void rotateZ(double u) {
        u = toRadians(u);
        double tmp = x;
        x = tmp * cos(u) - y * sin(u);
        y = tmp * sin(u) + y * cos(u);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }
}

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class MySlider extends JSlider implements Observer {
    protected Cube cube;
    protected JPanel panel;

    public MySlider(Cube cube, JPanel panel) {
        super(-180, 180, 0);
        this.cube = cube;
        this.panel = panel;
        this.setPaintTicks(true);
        this.setMajorTickSpacing(100);
        this.setPaintTrack(true);
    }

    public void update(Observable o, Object arg) {
        cube = (Cube) o;
        panel.repaint();
    }
}

import java.awt.*;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Observable;
import java.util.Random;
import java.util.StringTokenizer;

public class Cube extends Observable {
    private final Facet[] facets;
    private int isTransparent = 1;
    private int isOrthographic = 1;

    public Cube(String file) throws IOException {
        RandomAccessFile f = new RandomAccessFile(file, "r");
        StringTokenizer st = new StringTokenizer(f.readLine());

        R3Vector[] vertexes = new R3Vector[Integer.parseInt(st.nextToken())];
        facets = new Facet[Integer.parseInt(st.nextToken())];
        Random random = new Random();

        for (int i = 0; i < vertexes.length; i++) {
            st = new StringTokenizer(f.readLine());
            double x = Double.parseDouble(st.nextToken());
            double y = Double.parseDouble(st.nextToken());
            double z = Double.parseDouble(st.nextToken());
            vertexes[i] = new R3Vector(x, y, z);
        }
        int k = 0;
        for (int i = 0; i < facets.length; i++) {
            st = new StringTokenizer(f.readLine());
            int size = Integer.parseInt(st.nextToken());
            R3Vector[] facet = new R3Vector[size];
            facet[0] = vertexes[Integer.parseInt(st.nextToken()) - 1];
            for (int j = 1; j < size; j += 1) {
                facet[j] = vertexes[Integer.parseInt(st.nextToken()) - 1];
            }
            facets[i] = new Facet(facet);
            double hue = random.nextFloat();
            double saturation = (random.nextInt(2000) + 1000) / 10000f;
            double luminance = 0.9;
            facets[i].setColor(Color.getHSBColor((float) hue, (float) saturation, (float) luminance));
        }
    }

    public void scale(double scale) {
        for (Facet facet : facets) {
            facet.scale(scale);
        }
    }

    public void translate(double dx, double dy, double dz) {
        for (Facet facet : facets) {
            facet.translate(dx, dy, dz);
        }
    }

    public void rotate(double ux, double uy, double uz) {
        for (Facet facet : facets) {
            facet.rotate(ux, uy, uz);
        }
    }

    public void rotateVertically(int x) {
        for (Facet facet : facets) {
            facet.rotate(x, 0, 0);
        }
        this.setChanged();
        this.notifyObservers();
    }

    public void rotateHorizontally(int y) {
        for (Facet facet : facets) {
            facet.rotate(0, y, 0);
        }
        this.setChanged();
        this.notifyObservers();
    }

    public void draw(Graphics g) {
        for (Facet facet : facets) {
            facet.draw(g, isTransparent, isOrthographic);
        }
    }

    public void changeTransparency() {
        isTransparent *= -1;
    }

    public void changeOrth() {
        isOrthographic *= -1;
    }
}

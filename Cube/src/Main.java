import java.awt.*;
import java.io.IOException;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws IOException {
        Cube cube = new Cube("coordinates.txt");
        Random random = new Random();
        cube.scale(5);
//        cube.rotate(random.nextInt(360), random.nextInt(360), random.nextInt(360));
        CubeFrame cubeFrame = new CubeFrame(cube);
    }
}

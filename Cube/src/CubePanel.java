import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CubePanel extends JPanel {
    private final Cube cube;
    private final int width;
    private final int height;

    CubePanel(Cube cube, int width, int height) {
        this.setBackground(Color.green);
        this.width = width;
        this.height = height;
        this.cube = cube;
        this.setFocusable(true);
        this.setFocusTraversalKeysEnabled(false);
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                requestFocusInWindow();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                switch (keyCode) {
                    case KeyEvent.VK_UP -> {
                        cube.rotate(-1, 0, 0);
                        update(getGraphics());
                    }
                    case KeyEvent.VK_DOWN -> {
                        cube.rotate(1, 0, 0);
                        update(getGraphics());
                    }
                    case KeyEvent.VK_LEFT -> {
                        cube.rotate(0, 1, 0);
                        update(getGraphics());
                    }
                    case KeyEvent.VK_RIGHT -> {
                        cube.rotate(0, -1, 0);
                        update(getGraphics());
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, width, height);
        g.translate(width / 2 - 50, height / 2 - 50);
        cube.draw(g);
    }
}

import javax.swing.*;
import java.awt.*;

public class CubeController extends JPanel {

    public CubeController(Cube cube, Container pane, JPanel panel) {
        MyHorizontalSlider myHorizontalSlider = new MyHorizontalSlider(cube, panel);
        MyVerticalSlider myVerticalSlider = new MyVerticalSlider(cube, panel);
        myVerticalSlider.setPreferredSize(new Dimension(50, 400));
        myHorizontalSlider.setPreferredSize(new Dimension(400, 50));
        Container buttons = new Container();
        buttons.setLayout(new GridLayout(1, 2));
        MyTransparencyButton transparencyButton = new MyTransparencyButton(cube, panel);
        MyOrtButton ortButton = new MyOrtButton(cube, panel);
        buttons.add(transparencyButton);
        buttons.add(ortButton);
        pane.add(buttons, BorderLayout.PAGE_START);
        pane.add(myHorizontalSlider, BorderLayout.PAGE_END);
        pane.add(myVerticalSlider, BorderLayout.LINE_END);
        cube.addObserver(myHorizontalSlider);
        cube.addObserver(myVerticalSlider);
        cube.addObserver(transparencyButton);
        cube.addObserver(ortButton);
    }
}

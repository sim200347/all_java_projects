import java.util.Scanner;

import static java.lang.Math.*;

public class FirstProject {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int maximum = 0;
        int input;
        do {
            input = in.nextInt();
            maximum = max(maximum, input);
            System.out.println(maximum);
        } while (in.hasNextInt());
//        System.out.println(Integer.MIN_VALUE);
//        System.out.println(maximum);
    }
}

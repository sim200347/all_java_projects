//package bricks;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;

/* Контейнер кирпичей, которые еще не введены в игру. */
/* Игрок выиграл, если все кирпичи разбиты. */

public class BrickPile {
	/*
	 @_pf    - игровое поле
	 @_briks - множество кирпичей
	 @_rows  - количество линий кирпичей
	 @cols   - количество кирпичей в каждой линии
	 */

	private PlayField _pf;
	private Vector _bricks;
	private final int _rows = 5;
	private final int _cols = 5;

	public BrickPile(PlayField pf, Image[] bricks){
		_pf = pf;
		_bricks = new Vector(); //!!!
		int startx = 80;
		int x = startx, y = 150;
		Random random = new Random();

		for(int r = 0; r < _rows; r++){
			for(int c = 0; c < _cols; c++){
				Brick b = null;
				Rectangle pos = new Rectangle(x, y, bricks[2].getWidth(null), bricks[2].getHeight(null));
				switch (random.nextInt(0, 7)) {
					case 0 -> {
						b = new Brick(_pf, this, bricks[2], pos);
					}
					case 1 -> {
						b = new HardBrick(_pf, this, pos, bricks[2], bricks[3]);
					}
					case 2 -> {
						b = new PowerBrick(_pf, this, pos, bricks[4]);
					}
					case 3 -> {
						b = new WallBrick(_pf, this, pos, bricks[5]);
					}
					case 4 -> {
						b = new StickyBrick(_pf, this, pos, bricks[6]);
					}
					case 5 -> {
						b = new ArmorBrick(_pf, this, pos, bricks[7]);
					}
					case 6 -> {
						b = new TrapBrick(_pf, this, pos, bricks[8]);
					}
				}
				pf.addSprite(b);
				_bricks.addElement(b);
				// В зависимости от номера кирпича добавим на игровое поле
				// либо простой кирпич, либо крепкий.
//				if (((r+1) * (c+1)) % (_rows * _cols * 0.1) == 0 ) {
//					HardBrick hb = new HardBrick(_pf, this, pos, img1, img2);
//					 //!!!
//					pf.addSprite(hb);
//					_bricks.addElement(hb);
//				}
//				else  {
//					Brick b = new Brick(_pf, this, img1, pos);
//					pf.addSprite(b);
//					_bricks.addElement(b); //!!!
//				}

				x += bricks[2].getWidth(null);
			}

			y += bricks[2].getHeight(null) + 2;
			x = startx;
		}
	}

	public int unbrokenCount() {
		int result = 0;
		
		for (int i = 0; i < _bricks.size(); i++) {
			if ( !((Brick) _bricks.elementAt(i)).isDead() && !(isWall((Brick) _bricks.elementAt(i))))
				result++; 	
		}
		return result;
	}

	public int brokenCount() {
		return _bricks.size() - unbrokenCount();	
	}

	protected boolean isWall(Brick brick) {
		return brick.getClass() == WallBrick.class;
	}
}

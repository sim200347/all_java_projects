import java.awt.*;

public class ArmorBrick extends Brick {

    public ArmorBrick (PlayField pf, BrickPile bp, Rectangle p,
                      Image img) {
        super(pf, bp, img, p);
    }

    public void hitBy(Puck p) {
        if (p.getVelocity().getSpeedY() > 0) {
            _isDead = true;
            if (_bp.unbrokenCount() == 0) {
                _pf.getMatch().win();
            }
        }
        p.getVelocity().reverseY();
    }
}


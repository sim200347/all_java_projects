import java.awt.*;

public class PowerBrick extends Brick {
    private int _speedIncreasement = 2;

    public PowerBrick(PlayField pf, BrickPile bp, Rectangle p,
                      Image img) {
        super(pf, bp, img, p);
    }

    public void hitBy(Puck p) {
        _isDead = true;
        if (_bp.unbrokenCount() == 0) {
            _pf.getMatch().win();
        }
        p.getVelocity().reverseY();
        p.increaseSpeed(_speedIncreasement);
    }
}


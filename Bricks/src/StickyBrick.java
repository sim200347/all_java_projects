import java.awt.*;

public class StickyBrick extends Brick {
    private int sleepTime = 2000;

    public StickyBrick(PlayField pf, BrickPile bp, Rectangle p,
                     Image img) {
        super(pf, bp, img, p);
    }

    public void hitBy(Puck p) {
        _isDead = true;
        if (_bp.unbrokenCount() == 0) {
            _pf.getMatch().win();
        }
        p.getVelocity().reverseY();
        p.sleep(sleepTime);
    }
}


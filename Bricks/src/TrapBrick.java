import java.awt.*;

public class TrapBrick extends Brick {

    public TrapBrick (PlayField pf, BrickPile bp, Rectangle p,
                       Image img) {
        super(pf, bp, img, p);
    }

    public void hitBy(Puck p) {
        if (p.getVelocity().getSpeedY() < 0) {
            p.dead();
        } else
            _isDead = true;
        if (_bp.unbrokenCount() == 0) {
            _pf.getMatch().win();
        }
        p.getVelocity().reverseY();
    }
}


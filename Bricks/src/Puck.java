//package bricks;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* Шайба */

class Puck extends MovableSprite {
	/*
	 * @_ps - хранилище шайб, двунаправленая ассоциация
	 */
	 
	private PuckSupply _ps;

	
	public Puck(PlayField pf, PuckSupply ps, Image img) {
		super(
			pf,
			img,
			(new Rectangle(pf.getWidth()/2, pf.getHeight()/2, img.getWidth(pf), img.getHeight(pf))),
			90,
			10
		);
		
		_isMoving = true;
		_ps = ps;
	}

	public boolean checkPool() {
		return _ps.size() > 0;
	}

	public PuckSupply get_ps() {
		return _ps;
	}

	public void move() {
		if (!_isMoving)
			return;

		Rectangle b = _pf.getBoundary();

		_prevPos = _pos;
		_pos.translate(_v.getSpeedX(), _v.getSpeedY());
    
		/* Oбработка соударения sсо стенами, полом и потолком. */
		if (_pos.x <= b.x) {
			_pos.x = b.x;
			_v.reverseX();
		} else if (_pos.x + _pos.width >= b.width + b.x) {
			_pos.x = b.x + b.width - _pos.width;
			_v.reverseX();
		} else if (_pos.y <= b.y) {
			_pos.y = b.y;
			_v.reverseY();
		} else if (_pos.y + _pos.height > b.y + b.height) {
			/* Шайба упала на пол */
			_isDead = true;
			if (_ps.size() == 0) {
				_pf.getMatch().loose();
			} else {
				_pf.addSprite(_ps.get());
			}
		}
	
		/* Обработка соударения с другими спрайтами */
		if (collideWith() != null) {
			_pos = _prevPos;
			collideInto(collideWith());
		}
	}
	
	/* Реакция на возникновение коллизии. */
	public void collideInto(Sprite s) {
		s.hitBy(this);
	}
	
	public void hitBy(Puck p) {
		;
	}

	public void increaseSpeed(int accelerate) {
		_v.accelerate(accelerate);
	}

	public void setSpeed(int speed) {
		_v.setSpeed(speed);
	}

	public void sleep(int sleepTime) {
		SpriteVector spriteVector = _pf.get_sprites();
		setSpeed(0);
		final ScheduledExecutorService scheduler =
				Executors.newScheduledThreadPool(1);
		final Runnable resetSpeed = new Runnable() {
			@Override
			public void run() {
				spriteVector.update();
				setSpeed(10);
				setDirection(90);
			}
		};
		final ScheduledFuture<?> stickyHandle = scheduler.schedule(resetSpeed, 2, TimeUnit.SECONDS);
	}

	public void dead() {
		_isDead = true;
		if (!checkPool()) {
			_pf.getMatch().loose();
		} else {
			_pf.addSprite(_ps.get());
		}
	}
}

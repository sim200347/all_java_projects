public class Deq extends QueueDeq {

    public Deq() {
        this(DEFSIZE);
    }

    public Deq(int capacity) {
        array = new char[capacity];
        clear();
    }

    @Override
    public boolean empty() {
        return super.empty();
    }

    @Override
    public void clear() {
        super.clear();
    }

    public void pushFront(char val) throws Exception {
        if (++size > array.length)
            throw new Exception();
        array[head = backward(head)] = val;
    }

    public void pushBack(char val) throws Exception {
        super.push(val);
    }

    public int popFront() throws Exception {
        return super.pop();
    }

    public int popBack() throws Exception {
        int val = back();
        tail = backward(tail);
        size -= 1;
        return val;
    }

    protected int backward(int index) {
        return --index >= 0 ? index : size - 1;
    }

    @Override
    protected int forward(int index) {
        return super.forward(index);
    }

    @Override
    public char front() throws Exception {
        return super.front();
    }

    public char back() throws Exception {
        if (empty())
            throw new Exception();
        return array[tail];
    }
}

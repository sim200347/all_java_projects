import java.util.Scanner;

public class DeqTest {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        int size = s.length();
        Deq deqConvex = new Deq(size);
        for (int i = 0; i < size; i++) {
            try {
                deqConvex.pushBack(s.charAt(i));
            } catch (Exception ignored) {}
        }
        for (int i = 0; i < size; i++) {
            try {
                if (deqConvex.back() == deqConvex.front()) {
                    deqConvex.popBack();
                    deqConvex.popFront();
                } else {
                    System.out.println("Строка не является палиндромом");
                    return;
                }
            } catch (Exception ignored) {}
        }
        System.out.println("Строка является палиндромом");
    }
}

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class MyButton extends JButton implements Observer{
    protected Counter counter;
    protected MyPanel panel;

    public MyButton(Counter counter, MyPanel panel){
        this.counter = counter;
        this.panel = panel;
    }

    public void update(Observable o, Object arg) {
        counter = (Counter) o;
        panel.setStr(counter.getCounter());
        panel.repaint();
    }
}


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonMinus extends MyButton{

    public ButtonMinus(Counter counter, MyPanel panel) {
        super(counter, panel);
        MyButtonListener listener = new MyButtonListener();
        this.addActionListener(listener);
        this.setText("Minus");
    }

    private class MyButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            counter.decrement();
        }
    }
}

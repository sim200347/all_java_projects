import javax.swing.*;
import java.awt.*;

public class PaneWithButtons extends JPanel {

    PaneWithButtons(Counter counter, MyPanel panel) {
        this.setLayout(new GridLayout(2, 1));
        ButtonAdd buttonAdd = new ButtonAdd(counter, panel);
        ButtonMinus buttonMinus = new ButtonMinus(counter, panel);
        this.add(buttonAdd);
        this.add(buttonMinus);
        counter.addObserver(buttonAdd);
        counter.addObserver(buttonMinus);
    }
}

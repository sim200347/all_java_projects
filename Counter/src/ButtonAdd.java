import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonAdd extends MyButton {
    public ButtonAdd(Counter counter, MyPanel panel) {
        super(counter, panel);
        MyButtonListener listener = new MyButtonListener();
        this.addActionListener(listener);
        this.setText("Add");
    }

    private class MyButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            counter.increment();
        }
    }
}

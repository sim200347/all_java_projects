import java.util.Scanner;

public class StackTest {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        Stack stack = new Stack(s.length());
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            switch (ch) {
                case '{', '(', '[' -> {
                    try {
                        stack.push(ch);
                    } catch (Exception ignored) {}
                }
                case '}' -> {
                    try {
                        if (!stack.empty() && (stack.top() == '{')) {
                            stack.pop();
                        } else {
                            System.out.println("Неправильно");
                            return;
                        }
                    } catch (Exception ignored) {}
                }
                case ']' -> {
                    try {
                        if (!stack.empty() && (stack.top() == '[')) {
                            stack.pop();
                        } else {
                            System.out.println("Неправильно");
                            return;
                        }
                    } catch (Exception ignored) {}
                }
                case ')' -> {
                    try {
                        if (!stack.empty() && (stack.top() == '(')) {
                            stack.pop();
                        } else {
                            System.out.println("Неправильно");
                            return;
                        }
                    } catch (Exception ignored) {}
                }
            }
        }
        if (stack.empty()) {
            System.out.println("Правильно");
        } else {
            System.out.println("Неправильно");
        }
    }
}

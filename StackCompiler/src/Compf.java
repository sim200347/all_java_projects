import java.io.FileWriter;
import java.io.IOException;
import java.util.Stack;

public class Compf extends Stack<Character> {
    protected final static int SYM_LEFT = 0,
            SYM_RIGHT = 1,
            SYM_OPER = 2,
            SYM_OTHER = 3;

    protected FileWriter fw = new FileWriter("answer.txt", false);
    protected boolean isStart = true;

    public Compf() throws IOException {
    }

    protected int symType(char c) throws IOException {
        return switch (c) {
            case '(', '[', '{' -> SYM_LEFT;
            case ')', ']', '}' -> SYM_RIGHT;
            case '+', '-', '*', '/' -> SYM_OPER;
            default -> symOther(c);
        };
    }

    private void processSymbol(char c) throws IOException {
        switch (symType(c)) {
            case SYM_LEFT -> {
                isStart = true;
                push(c);
            }
            case SYM_RIGHT -> {
                isStart = true;
                processSuspendedSymbols(c);
                pop();
            }
            case SYM_OPER -> {
                isStart = true;
                processSuspendedSymbols(c);
                push(c);
            }
            case SYM_OTHER -> nextOther(c);
        }
    }

    private void processSuspendedSymbols(char c) throws IOException {
        while(precedes(peek(), c))
            nextOper(pop());
    }

    protected int priority(char c){
        return c == '+' || c == '-' ? 1 : 2;
    }

    protected boolean precedes(char a, char b) throws IOException {
        if(symType(a) == SYM_LEFT) return false;
        if(symType(b) == SYM_RIGHT) return true;
        return priority(a) >= priority(b);
    }

    protected int symOther(char c) {
        if (c < 'a' || c > 'z'){
            System.out.println("Недопустимый символ: " + c);
            System.exit(0);
        }
        return SYM_OTHER;
    }

    protected void nextOper(char c) throws IOException {
        fw.append(String.valueOf(c)).append(" ");
    }

    protected void nextOther(char c) throws IOException {
        nextOper(c);
    }

    public void compile(char[] str) throws IOException {
        processSymbol('(');
        for (char c : str)
            processSymbol(c);
        processSymbol(')');
    }
}
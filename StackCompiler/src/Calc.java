import java.io.IOException;

public class Calc extends Compf {
    private StackInt s;

    public Calc() throws IOException {
        s = new StackInt();
    }

    protected static int char2int(char c) {
        return (int) c - (int) '0';
    }

    @Override
    protected int symOther(char c) {
        if (c < '0' || c > '7') {
            throw new IllegalArgumentException("Numbers must be between 0 and 7");
        }
        return SYM_OTHER;
    }

    @Override
    protected void nextOper(char c) {
        int second = s.pop();
        int first = s.pop();
        switch (c) {
            case '+' -> s.push(first + second);
            case '-' -> s.push(first - second);
            case '*' -> s.push(first * second);
            case '/' -> s.push(first / second);
        }
    }

    @Override
    protected void nextOther(char c) {
        if (!isStart) {
            s.push(s.pop() * 8 + char2int(c));
        } else {
            isStart = false;
            s.push(char2int(c));
        }
    }

    @Override
    public final void compile(char[] str) throws IOException {
        super.compile(str);
        fw.write(String.format("%d", s.top()));
        fw.close();
    }
}
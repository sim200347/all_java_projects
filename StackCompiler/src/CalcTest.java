import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.IOException;

public class CalcTest{
    @Test
    @DisplayName("Test calculations")
    public void testResults() throws IOException {
        char[] output = new char[9];
        Calc calc = new Calc();
        char[] input = {'3', '*', '3'};
        calc.compile(input);
        try {
            FileReader fr = new FileReader("answer.txt");
            fr.read(output);
            fr.close();
        } catch (Exception ignored) {
        }
        Assertions.assertEquals('9', output[0]);
        input = new char[]{'9', '+', '9'};
        char[] finalInput = input;
        Assertions.assertThrows(IllegalArgumentException.class, () -> calc.compile(finalInput));
        input = new char[]{'a', '+', '9'};
        char[] finalInput1 = input;
        Assertions.assertThrows(IllegalArgumentException.class, () -> calc.compile(finalInput1));
    }

    @Test
    @DisplayName("Test conversion from char to int")
    public void testChar2int() {
        Assertions.assertEquals(9, Calc.char2int('9'));
    }

    @Test
    @DisplayName("Test character legibility")
    public void testSymOther() throws IOException {
        Calc calc = new Calc();
        Assertions.assertEquals(Compf.SYM_OTHER, calc.symOther('5'));
        Assertions.assertThrows(IllegalArgumentException.class, () -> calc.symOther('9'));
        Assertions.assertThrows(IllegalArgumentException.class, () -> calc.symOther('a'));
    }

    @Test
    @DisplayName("Test character types")
    public void testSymType() throws IOException {
        Calc calc = new Calc();
        Assertions.assertEquals(Compf.SYM_LEFT, calc.symType('('));
        Assertions.assertEquals(Compf.SYM_RIGHT, calc.symType(')'));
        Assertions.assertEquals(Compf.SYM_OPER, calc.symType('*'));
        Assertions.assertEquals(Compf.SYM_OTHER, calc.symType('5'));
    }

    @Test
    @DisplayName("Test operations priority")
    public void testPriority() throws IOException {
        Calc calc = new Calc();
        Assertions.assertEquals(1, calc.priority('+'));
        Assertions.assertEquals(2, calc.priority('*'));
    }

    @Test
    @DisplayName("Test if calculations needed")
    public void testPrecedes() throws IOException {
        Calc calc = new Calc();
        Assertions.assertTrue(calc.precedes('+', ')'));
        Assertions.assertTrue(calc.precedes('*', '*'));
        Assertions.assertTrue(calc.precedes('/', ')'));
        Assertions.assertFalse(calc.precedes('(', '*'));
        Assertions.assertFalse(calc.precedes('(', '('));
        Assertions.assertFalse(calc.precedes('+', '*'));
    }


}
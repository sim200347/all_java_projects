import java.util.Scanner;

import static java.lang.Math.*;

public class ClassWork {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        int minimum = Integer.MAX_VALUE;
        str = str.replaceAll("[ ]+", " ").strip();
        String[] strings = str.split(" ");
        for (String string : strings) {
            minimum = min(string.length(), minimum);
        }
        for (String string : strings) {
            if (string.length() == minimum) {
                System.out.println(string);
            }
        }
    }
}

import java.util.Scanner;

import static java.lang.Math.abs;

public class Recursion {
    public static int fact(int n) {
        return n <= 1 ? n : n*fact(n-1);
    }

    public static int fib(int n) {
        return n <= 1 ? n : fib(n-1) + fib(n - 2);
    }

    public static int multi(int a, int b) {
        return b == 0 ? 0 : a + multi(a, b - 1);
    }

    public static int power(int a, int b) {
        return b == 0 ? 1 : a * power(a, b - 1);
    }

    public static int powerWithoutMultiplication(int a, int b) {
        return b == 0 ? 1 : multi(a, power(a, b - 1));
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(fact(n));
        n = in.nextInt();
        System.out.println(fib(n));
        int a = in.nextInt();
        int b = in.nextInt();
        if (a >= 0 && b <= 0) {
            System.out.println(-multi(a, abs(b)));
        } else if (a <= 0 && b >= 0) {
            System.out.println(-multi(abs(a), b));
        } else {
            System.out.println(multi(abs(a), abs(b)));
        }
        a = in.nextInt();
        b = in.nextInt();
        if (a > 0 && b > 0) {
            System.out.println(power(a, b));
        }
        if (a > 0) {
            if (b > 0) {
                System.out.println(powerWithoutMultiplication(a, b));
            } else System.out.println(1.0/powerWithoutMultiplication(a, abs(b)));
        } else System.out.println("Wrong input");
        if (a > 0) {
            if (b > 0) {
                System.out.println(powerWithoutMultiplication(a, b));
            } else System.out.println(1.0/powerWithoutMultiplication(a, abs(b)));
        } else System.out.println("Wrong input");
    }
}

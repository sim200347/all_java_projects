import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class Files {
    public static void main(String[] args) throws IOException {
        try (FileReader fr = new FileReader("test.txt");
             FileWriter fw = new FileWriter("answer.txt");
             Scanner in = new Scanner(fr)) {
            int[] data = new int[127];
            while (in.hasNextLine()) {
                String s = in.nextLine();
                for (int i = 0; i < s.length(); i++) {{
                    data[s.charAt(i)]++;
                    }
                }
            }
            for (int i = data.length - 1; i > 0; i--) {
                if (data[i] != 0) {
                    fw.write((char)(i) + " - " + data[i] + "\n");
                }
            }
        } catch (IOException e) {
            throw new IOException("Error in files, check their paths and types");
        }
    }
}

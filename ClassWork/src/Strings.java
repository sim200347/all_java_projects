import java.util.Scanner;

public class Strings {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        int count = 0;
        String[] data = s.split(" ");
        for (String datum : data) {
            if (datum.charAt(0) == 'a') {
                count++;
            }
        }
        System.out.println(count);
    }
}
import java.util.Scanner;

import static java.lang.Math.max;


public class FirstTest {

    public static boolean is_equal(int a, int b, int c) {
        return a == b || a == c || b == c;
    }

    public static int maximum(int a, int b, int c) {
        int tmp = max(a, b);
        return max(tmp, c);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        System.out.println("Task 1: " + is_equal(a, b, c));
        System.out.println("Task 2: " + maximum(a, b, c));
    }
}

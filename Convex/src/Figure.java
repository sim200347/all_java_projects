import java.awt.*;

interface Figure {
    int pointSize = 6;
    public double perimeter();
    public double area();
    public double minDiagonal();
    public Figure add(R2Point p);
    public void draw(Graphics g);
}
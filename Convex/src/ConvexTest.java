class ConvexTest{
    public static void main(String[] args) throws Exception{
        Convex convex = new Convex();
        ConvexFrame frame = new ConvexFrame(convex);
        while(true){
            convex.add(new R2Point());
            frame.repaint();
            System.out.println("S = " + convex.area() + ", P = " + convex.perimeter() + ", Min diagonal = " + convex.minDiagonal());
        }
    }
}
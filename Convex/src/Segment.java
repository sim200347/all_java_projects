import java.awt.*;

class Segment implements Figure{
    private R2Point p, q;

    public Segment(R2Point p, R2Point q){
        this.p = p;
        this.q = q;
    }

    public double perimeter(){
        return 2.0 * R2Point.dist(p, q);
    }

    public double area(){
        return 0.0;
    }

    public double minDiagonal() {
        return 0;
    }

    public Figure add(R2Point r){
        if(R2Point.isTriangle(p, q, r))
            return new Polygon(p, q, r);
        if(q.inside(p, r))
            q = r;
        if(p.inside(r, q))
            p = r;
        return this;
    }

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillOval(p.getX() - pointSize/2, p.getY() - pointSize/2, pointSize, pointSize);
        g.drawLine(p.getX(), p.getY(), q.getX(), q.getY());
        g.fillOval(q.getX() - pointSize/2, q.getY() - pointSize/2, pointSize, pointSize);
    }
}
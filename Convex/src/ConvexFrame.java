import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

public class ConvexFrame extends JFrame {
    public static final int width = 800;
    public static final int height = 900;

    private Convex convex;

    private BufferStrategy strategy = this.getBufferStrategy();

    public ConvexFrame(Convex convex){
        start();
        this.convex = convex;
    }

    private void start(){
        this.setSize(width, height);
        this.setTitle("Convex");
        this.setLocation(width/2, height/2);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.createBufferStrategy(2);
        strategy = this.getBufferStrategy();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) strategy.getDrawGraphics();
        g2d.setColor(Color.white);
        g2d.clearRect(0, 0, width, height);
        g2d.setColor(Color.BLACK);
        g2d.translate(width/2, height/2);
        g2d.drawLine(-width/2, 0, width/2, 0);
        g2d.drawLine(0, height/2, 0, -height/2);
        g2d.scale(1, -1);
        convex.draw(g2d);
        g2d.dispose();
        strategy.show();
    }

    @Override
    public void update(Graphics g) {
        paint(g);
    }
}

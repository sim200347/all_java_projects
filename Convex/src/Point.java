import java.awt.*;

class Point implements Figure{
    private R2Point p;

    public Point(R2Point p){
        this.p = p;
    }

    public double perimeter(){
        return 0.0;
    }

    public double area(){
        return 0.0;
    }

    public double minDiagonal() {
        return 0;
    }

    public Figure add(R2Point q){
        if(!p.equals(q))
            return new Segment(p, q);
        else
            return this;
    }

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillOval(p.getX() - pointSize/2, p.getY() - pointSize/2, pointSize, pointSize);
    }
}
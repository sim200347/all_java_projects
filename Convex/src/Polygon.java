import java.awt.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;

import static java.lang.Math.min;

class Polygon extends ArrayDeque implements Figure {
    private double s, p;

    public Polygon(R2Point a, R2Point b, R2Point c) {
        addFirst(b);
        if (b.light(a, c)) {
            addFirst(a);
            addLast(c);
        } else {
            addFirst(c);
            addLast(a);
        }
        p = R2Point.dist(a, b) + R2Point.dist(b, c) + R2Point.dist(c, a);
        s = Math.abs(R2Point.area(a, b, c));
    }

    public double perimeter() {
        return p;
    }

    public double area() {
        return s;
    }

    public double minDiagonal() {
        double minDiagonal = Integer.MAX_VALUE;
        if (size() > 3) {
            R2Point curPoint = (R2Point) removeFirst();
            for (R2Point p : (Iterable<R2Point>) this) {
                if (p != peekFirst() && p != peekLast()) {
                    minDiagonal = min(minDiagonal, R2Point.dist(curPoint, p) / 2);
                }
            }
            addFirst(curPoint);
            return minDiagonal;
        } else {
            return 0;
        }
    }

    private void grow(R2Point a, R2Point b, R2Point t) {
        p -= R2Point.dist(a, b);
        s += Math.abs(R2Point.area(a, b, t));
    }

    public Figure add(R2Point p) {
        int i;
        for (i = size(); i > 0 && !p.light((R2Point) getLast(), (R2Point) getFirst()); i--)
            addLast(removeFirst());
        if (i > 0) {
            R2Point x;
            grow((R2Point) getLast(), (R2Point) getFirst(), p);
            for (x = (R2Point) removeFirst(); p.light(x, (R2Point) getFirst()); x = (R2Point) removeFirst())
                grow(x, (R2Point) getFirst(), p);
            addFirst(x);
            for (x = (R2Point) removeLast(); p.light((R2Point) getLast(), x); x = (R2Point) removeLast())
                grow((R2Point) getLast(), x, p);
            addLast(x);
            this.p += R2Point.dist((R2Point) getLast(), p) + R2Point.dist(p, (R2Point) getFirst());
            addFirst(p);
        }
        return this;
    }

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        ArrayList<Integer> x = new ArrayList<>();
        ArrayList<Integer> y = new ArrayList<>();
        forEach((element) -> {
            R2Point point = (R2Point) element;
            x.add(point.getX());
            y.add(point.getY());
            g.fillOval(point.getX() - pointSize/2, point.getY() - pointSize/2, pointSize, pointSize);
        });
        g.drawPolygon(x.stream().mapToInt(i -> i).toArray(), y.stream().mapToInt(i -> i).toArray(), size());
    }
}
public class RecursCompiler {
    private static final int DEFSIZE = 256;
    private char[] str;
    private int index; //Число обработанных символов.

    public RecursCompiler() {
        str = new char[DEFSIZE];
    }

    private void compileF() throws Exception {
        if (str[index] == '-' || str[index] == '+')
            System.out.print(str[index++]);
        compileT();
        if (index >= str.length)
            return;
        if (str[index] == '+' || str[index] == '-') {
            char tmp = str[index++];
            compileF();
            compileT();
            System.out.print(tmp + " ");
        }
    }

    private void compileT() throws Exception {
        if (index >= str.length)
            return;
        compileM();
        if (str[index] == '*' || str[index] == '/') {
            char tmp = str[index++];
            compileM();
            compileT();
            System.out.print(tmp + " ");
        }
    }

    private void compileM() throws Exception {
        if (index >= str.length)
            return;
        switch (str[index]) {
            case ('(') -> {
                index++;
                compileF();
                if (str[index] == ')')
                    index++;
                else
                    throw new Exception("Brackets are wrong");
            }
            case ('{') -> {
                index++;
                compileF();
                if (str[index] == '}')
                    index++;
                else
                    throw new Exception("Brackets are wrong");
            }
            case ('[') -> {
                index++;
                compileF();
                if (str[index] == ']')
                    index++;
                else
                    throw new Exception("Brackets are wrong");
            }
            default -> {
                compileV();
            }
        }
    }

    private void compileV() {
        if (index >= str.length)
            return;
        if ('a' <= str[index] && str[index] <= 'z') {
            System.out.print(str[index++]);
            compileV();
        }
        System.out.print(" ");
    }

    public void compile(char[] str) throws Exception {
        this.str = str;
        index = 0;
        compileF();
        System.out.println();
    }
}
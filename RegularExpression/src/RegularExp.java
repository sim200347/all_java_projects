import java.util.Scanner;

public class RegularExp {
    public static void main(String[] args) {
        String regex1 = "";
        String regex2 = "a";
        String regex3 = "ab";
        String regex4 = "a|b";
        String regex5 = "a*";
        String regex6 = "a+";
        String regex7 = "aa*"; //Эквивалент regex6.
        String regex8 = "\\**";
        String regex9 = "a*b*";
        String regex10 = "aa*bb*";
        String regex11 = "a*|b*";
        String regex12 = "(a|b)*";
        String regex13 = "a|b*";
        String regex14 = "(aab|ab)*";
        Scanner in = new Scanner(System.in);
        while(true) {
            String input = in.nextLine();
            boolean result = input.matches("(-0|0)|((-|)(1|2|3|4|5|6|7|8|9)(0|1|2|3|4|5|6|7|8|9)*)\\.(0|1|2|3|4|5|6|7|8|9)*"); //Проверка строки на соответсвие регулярному выражению (языку).
            System.out.println(result);
        }
    }
}
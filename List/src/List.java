import java.util.Scanner;

public class List {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = 5;
        L2ListTemplates list = new L2ListTemplates(size);
        for (int i = 0; i < size; i++) {
            try {
                list.insertFront(in.nextInt());
            } catch (Exception ignored) {
                System.out.println("Error");
            }
        }
        list.toBack();
        for (int i = 0; i < size; i++) {
            try {
                System.out.println(list.before());
                list.backward();
            } catch (Exception ignored) {
                System.out.println("Error");
            }
        }
        list.toFront();
        for (int i = 0; i < size; i++) {
            try {
                System.out.println(list.after());
                list.forward();
            } catch (Exception ignored) {
                System.out.println("Error");
            }
        }
    }
}

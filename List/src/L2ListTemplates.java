public class L2ListTemplates extends L1List {
    protected int[] prev;
    protected int nilListBack;
    protected int nilFreeBack;

    L2ListTemplates() {
        this(DEFSIZE);
    }

    L2ListTemplates(int size) {
        super(size);
        prev = new int[size + 2];
        nilList = size;
        nilFree = size + 1;
        linkBack(nilListBack, nilListBack);
        for (int i = size - 1; i >= 0; i--)
            linkBack(i, i + 1);
        linkBack(size - 1, nilFreeBack);
    }

    protected void linkBack(int first, int second) {
        prev[first] = second;
    }

    protected int mallocIndexBack() {
        int index = prev[nilFreeBack];
        link(nilFreeBack, prev[index]);
        return index;
    }

    protected void freeIndexBack(int index) {
        link(index, prev[nilFreeBack]);
        link(nilFreeBack, index);
    }

    @Override
    public boolean empty() {
        return super.empty();
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void toFront() {
        super.toFront();
    }

    public void toBack() {
        after = nilList;
        before = prev[nilList];
    }

    public  boolean begin() {
        return before == 0;
    }

    @Override
    public boolean end() {
        return super.end();
    }

    @Override
    public void forward() throws Exception {
        super.forward();
    }

    public void backward() throws Exception {
        if(before == -1)
            throw new Exception();
        after = before;
        before = prev[before];
    }

    @Override
    public int after() throws Exception {
        return super.after();
    }

    public int before() throws Exception {
        return array[before];
    }

    public void insertBack(int val) throws Exception {
        int index = mallocIndexBack();
        link(before, index);
        link(index, after);
        after = index;
        array[index] = val;
    }

    public void insertFront(int val) throws Exception {
        super.insert(val);
    }

    public int eraseBack() throws Exception {
        int val = array[before];
        int index = before;
        before = prev[index];
        linkBack(after, before);
        freeIndexBack(index);
        return val;
    }

    public int eraseFront() throws Exception {
        return super.erase();
    }
}
